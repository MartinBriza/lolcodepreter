#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include "support.h"

int lolErrno = 0;

const char *lolErrorMsgs[] = {
    [LERR_OUT_OF_MEMORY] = "Out of memory",
    [LERR_MEMORY_CORRUPTION] = "Memory corruption",
    [LERR_LEXICAL_ERROR] = "Lexical error",
};

// TODO: 0 isn't an error, change this
void _lolError(const char *file, const char *function, const int line) {
    fprintf(stderr, "Error: %s:%s:%d: %s. Halt\n", file, function, line,
            lolErrno ? lolErrorMsgs[lolErrno] : "undefined");
    exit(1);
}

void _lolWarning(const char *file, const char *function, const int line, char *warning, ...) {
    va_list ap;
    fprintf(stderr, "Warning: %s:%s:%d: ", file, function, line);
    va_start(ap, warning);
    vfprintf(stderr, warning, ap);
    va_end(ap);
    fprintf(stderr, ".\n");
}
