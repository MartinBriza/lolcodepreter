#ifndef SUPPORT_H
#define SUPPORT_H

extern int lolErrno;

enum lolErrnos {
    LERR_WENT_BETTER_THAN_EXPECTED = 0,
    LERR_OUT_OF_MEMORY,
    LERR_MEMORY_CORRUPTION,
    LERR_LEXICAL_ERROR,
};

#define lolError() (_lolError((__FILE__), (__FUNCTION__), (__LINE__)))
#define lolErrorNo(number) {lolErrno = (number); lolError();}
#define lolWarning(warning...) _lolWarning((__FILE__), (__FUNCTION__), \
    (__LINE__), warning)

void _lolError(const char *file, const char *function, const int line);
void _lolWarning(const char *file, const char *function, const int line, char *warning, ...);

#endif // SUPPORT_H
