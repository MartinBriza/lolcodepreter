#include "array.h"
#include "../memory/memory.h"
#include "../misc.h"

array_t *array_init(size_t size) {
    array_t *ptr = lolMalloc(sizeof(array_t));
    ptr->size = size;
    ptr->ptr = lolMalloc(size);
    return ptr;
}

void *array_checkBounds(array_t *ptr, size_t pos) {
    if (pos >= ptr->size) {
        size_t newsize = ptr->size + (ENSURE_NONZERO(ptr->size >> 3) << 1);
        void *newptr = lolRealloc(ptr->ptr, newsize);
        ptr->ptr = newptr;
    }
    return ptr->ptr;
}
