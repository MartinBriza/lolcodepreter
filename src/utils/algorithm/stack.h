#ifndef STACK_H
#define STACK_H

#include <stddef.h>
#include <stdbool.h>
#include "array.h"

typedef struct {
    array_t array;
    size_t tip;
} stack_t;

stack_t *stack_init(size_t size);
void stack_pushCopy(stack_t *stack, void *item, size_t size);
void stack_pushChar(stack_t *stack, char item);
void stack_flush(stack_t *stack);
bool stack_isEmpty(stack_t *stack);

#endif // STACK_H
