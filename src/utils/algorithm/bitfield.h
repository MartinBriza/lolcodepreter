#ifndef BITFIELD_H
#define BITFIELD_H

#define BITFIELD_GET(field, index) ( \
(field[index / 8]) & (1 << (index % 8))\
)
#define BITFIELD_JUMP(field, index) { \
(field[index / 8]) |= (1 << (index % 8))\
}
#define BITFIELD_KILL(field, index) { \
(field[index / 8]) &= ~(1 << (index % 8))\
}
#define BITFIELD_INIT(field, size) ( \
memset(field, 0, (size + 7) / 8); \
)
#define BITFIELD_ALLOCINIT(size) ( \
lolCalloc((size + 7) / 8, 1) \
)

#endif // BITFIELD_H
