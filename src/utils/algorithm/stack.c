#include <string.h>
#include <stdbool.h>

#include "stack.h"
#include "../memory/memory.h"

stack_t *stack_init(size_t size) {
    stack_t *ptr = lolMalloc(sizeof(stack_t));
    array_t *innerArray = (array_t *) ptr;
    innerArray->size = size;
    innerArray->ptr = lolMalloc(size);
    ptr->tip = 0;
    return ptr;
}

void stack_pushCopy(stack_t *stack, void *item, size_t size) {
    array_checkBounds((array_t *) stack, stack->tip + size);
    memcpy(stack->array.ptr + stack->tip, item, size);
    stack->tip += size;
}

void stack_pushChar(stack_t *stack, char item) {
    array_checkBounds((array_t *) stack, stack->tip + 1);
    char *ptr = stack->array.ptr;
    ptr[stack->tip] = item;
    stack->tip++;
}

void stack_flush(stack_t *stack) {
    if (stack->array.ptr) {
        ((char *) stack->array.ptr)[0] = 0;
    }
    stack->tip = 0;
}

bool stack_isEmpty(stack_t *stack) {
    return stack->tip == 0;
}