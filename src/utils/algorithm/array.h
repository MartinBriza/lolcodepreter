#ifndef ARRAY_H
#define ARRAY_H

#include <stddef.h>

typedef struct {
    void *ptr;
    size_t size;
} array_t;

array_t *array_init(size_t size);
void *array_checkBounds(array_t *ptr, size_t pos);

#endif // ARRAY_H
