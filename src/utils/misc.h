#ifndef MISC_H
#define MISC_H

#define ENSURE_NONZERO(x) ((x) == 0 ? 1 : x)

#endif // MISC_H
