#ifndef MEMORY_ALLOC_H
#define MEMORY_ALLOC_H

void lolMemoryInit();
void lolMemoryDestroy();

#define lolMalloc(size) _lolMalloc(size)
void *_lolMalloc(size_t size);
#define lolCalloc(nmemb, size) _lolCalloc((nmemb), (size))
void *_lolCalloc(size_t nmemb, size_t size);
#define lolFree(ptr) _lolFree(ptr)
void _lolFree(void *ptr);
#define lolRealloc(ptr, size) _lolRealloc(ptr, size)
void *_lolRealloc(void *ptr, size_t size);

#endif // MEMORY_ALLOC_H
