#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "alloc.h"
#include "../support.h"

#define STORAGE_SIZE 256
#define TABLE_INDEX(x) ((  ((uintptr_t) (x)) \
                         + ((uintptr_t) (x) >> 8) \
                         + ((uintptr_t) (x) >> 16) \
                         + ((uintptr_t) (x) >> 24) \
                        ) & (STORAGE_SIZE - 1) \
                       )

struct memoryFile {
    void *ptr;
    struct memoryFile *next;
} *pointerStorage[STORAGE_SIZE];

void lolMemoryInit() {
    memset(pointerStorage, (intptr_t) NULL, sizeof(pointerStorage));
}

void lolMemoryDestroy() {
    struct memoryFile *file;
    struct memoryFile *tmp;
    for (size_t i = 0; i < STORAGE_SIZE; i++) {
        file = pointerStorage[i];
        tmp = NULL;
        while (file) {
            tmp = file;
            file = file->next;
            free(tmp->ptr);
            free(tmp);
        }
        if (file) {
            free(file->ptr);
            free(file);
        }
    }
    lolMemoryInit();
}

static void lolMemoryAdd(void *ptr) {
    struct memoryFile *file = pointerStorage[TABLE_INDEX(ptr)];
    struct memoryFile *previous = NULL;
    while (file) {
        previous = file;
        file = file->next;
    }
    file = calloc(sizeof(struct memoryFile), 1);
    if (!file) {
        lolErrno = LERR_OUT_OF_MEMORY;
        lolError();
    }
    file->ptr = ptr;
    if (previous) {
        previous->next = file;
    }
    else {
        pointerStorage[TABLE_INDEX(ptr)] = file;
    }
}

static void lolMemoryRem(void *ptr) {
    struct memoryFile *file = pointerStorage[TABLE_INDEX(ptr)];
    struct memoryFile *previous = NULL;
    if (!ptr) { // freeing null pointers is cool
        return;
    }
    while (file && file->ptr != ptr) {
        previous = file;
        file = file->next;
    }
    if (!file) { // couldn't find the file in the table
        lolErrorNo(LERR_MEMORY_CORRUPTION);
    }
    else {
        if (previous) {
            previous->next = file->next;
            free(file);
        }
        else {
            pointerStorage[TABLE_INDEX(ptr)] = NULL;
            free(file);
        }
    }
}

void *_lolMalloc(size_t size) {
    void *ptr = malloc(size);
    if (!ptr) {
        lolErrorNo(LERR_OUT_OF_MEMORY);
    }
    lolMemoryAdd(ptr);
    return ptr;
}

void *_lolCalloc(size_t nmemb, size_t size) {
    void *ptr = calloc(nmemb, size);
    if (!ptr) {
        lolErrorNo(LERR_OUT_OF_MEMORY);
    }
    lolMemoryAdd(ptr);
    return ptr;
}

void _lolFree(void *ptr) {
    lolMemoryRem(ptr);
    free(ptr);
}

void *_lolRealloc(void *ptr, size_t size) {
    void *newptr = realloc(ptr, size);
    if (!newptr) {
        free(ptr);
        lolErrorNo(LERR_OUT_OF_MEMORY);
    }
    lolMemoryRem(ptr);
    lolMemoryAdd(newptr);
    return newptr;
}
