#include "memory.h"
#include "../support.h"

#define MEMPOOL_STEP 8192

static void *memPool = NULL;
static ptrdiff_t memPoolOffset = 0;
static size_t memPoolSize = MEMPOOL_STEP;

void lolPoolInit(void) {
    if (memPool) {
        lolWarning("Was called twice, leaking memory");
    }
    memPool = lolMalloc(memPoolSize);
}

void lolPoolDispose(void) {
    free(memPool);
    memPool = NULL;
    memPoolOffset = 0;
    memPoolSize = MEMPOOL_STEP;
}

void *lolPoolAlloc(size_t size) {
    if (memPoolSize < memPoolOffset + size) {
        memPoolSize = ((memPoolOffset + size + MEMPOOL_STEP - 1) / MEMPOOL_STEP)
                       * MEMPOOL_STEP;
        memPool = lolRealloc(memPool, memPoolSize);
    }
    memPoolOffset += size;
    return memPool + memPoolOffset - size;
}

void lolPoolFree(void *ptr) {
    if (ptr < memPool) {
        lolWarning("Called with %p but pool is at %p\n", ptr, memPool);
    }
    else if (ptr > memPool + memPoolSize) {
        lolWarning("Called with a pointer that is %t after the"
                   "beginning of the pool but its size is only %t\n",
                   ptr - memPool, memPoolSize);
    }
    else {
        memPoolOffset = ptr - memPool;
    }
}
