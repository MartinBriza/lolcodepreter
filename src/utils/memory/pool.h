#ifndef MEMORY_POOL_H
#define MEMORY_POOL_H

#include <stddef.h>

void lolPoolInit(void);
void lolPoolDispose(void);
void *lolPoolAlloc(size_t size);
void lolPoolFree(void *ptr);

#endif // MEMORY_POOL_H
