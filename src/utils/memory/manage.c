#include <string.h>
#include "manage.h"
#include "alloc.h"

void *lolMemdup(void *src, size_t len) {
    void *dst = lolMalloc(len);
    return memcpy(dst, src, len);
}

void *lolStrdup(char *src) {
    size_t length = strlen(src);
    return lolMemdup(src, length + 1);
}