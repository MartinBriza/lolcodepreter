#ifndef MEMORY_MANAGE_H
#define MEMORY_MANAGE_H

void *lolMemdup(void *src, size_t len);
void *lolStrdup(char *src);

#endif // MEMORY_MANAGE_H