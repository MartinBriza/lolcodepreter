#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#include "utils/utils.h"

enum tokenTypes {
    T_NOTYPE,
    T_KEYWORD,
    T_WORD,
    T_NUMBER,
    T_STRING,
    T_CONTROL
};

enum controlSymbols {
    C_QUESTION,
    C_EXCLAMATION,
    C_SEPARATOR,
};

/**
 * Keyword types used in the language
 *
 * @warning if there are words those contain other words as their beginning, you
 * MUST file them AFTER the shorter ones - e.g. KTHXBYE has to be after KTHX.
 * That all just because I'm too damn lazy.
 */
enum keywordTypes {
    K_NOTYPE,
    K_CANHAS,
    K_READ,
    K_LINE,
    K_WORD,
    K_LETTER,
    K_IFILE,
    K_PBEGIN,
    K_END,
    K_PEND,
    K_ERROR,
    K_BYES,
    K_LOOP,
    K_PRINT,
    K_VAR,
    K_ITZ,
    K_LOL,
    K_IZ,
    K_TRUE,
    K_FALSE,
    K_GT,
    K_LT,
    K_THAN,
    K_EQ,
    K_NOT,
    K_AND,
    K_OR,
    K_XOR,
    K_ADD,
    K_INC,
    K_SUB,
    K_DEC,
    K_MUL,
    K_TMUL,
    K_DIV,
    K_TDIV,
    K_BTW,
    K_LAST_KEYWORD, /// a stop for iterations
};

const char const* keywords[] = {
    [K_NOTYPE] = "",
    [K_CANHAS] = "CAN HAS",
    [K_READ] =   "GIMMEH",
    [K_LINE] =   "LINE",
    [K_WORD] =   "WORD",
    [K_LETTER] = "LETTAR",
    [K_IFILE] =  "OUTTA",
    [K_PBEGIN] = "HAI",
    [K_PEND] =   "KTHXBYE",
    [K_ERROR] =  "DIAF",
    [K_BYES] =   "BYES",
    [K_END] =   "KTHX",
    [K_LOOP] =   "IM IN YR",
    [K_PRINT] =  "VISIBLE",
    [K_VAR] =    "I HAS A",
    [K_ITZ] =    "ITZ",
    [K_LOL] =    "LOL",
    [K_IZ] =     "IZ",
    [K_TRUE] =   "YARLY",
    [K_FALSE] =  "NOWAI",
    [K_GT] =     "BIGR",
    [K_LT] =     "SMALR",
    [K_THAN] =   "THAN",
    [K_EQ] =   "LIEK",

    [K_NOT] =    "NOT",
    [K_AND] =    "AND",
    [K_OR] =     "OR",
    [K_XOR] =    "XOR",

    [K_ADD] =    "UP",
    [K_INC] =    "UPZ",
    [K_SUB] =    "NERF",
    [K_DEC] =    "NERFZ",
    [K_MUL] =    "TIEMZ",
    [K_TMUL] =   "TIEMZD",
    [K_DIV] =    "OVAR",
    [K_TDIV] =   "OVARZ",

    [K_BTW] =    "BTW",
};

typedef struct {
    enum tokenTypes type;
    union {
        int64_t num;
        void *ptr;
    };
} token_t;

enum storemanActions {
    SA_NONE,
    SA_DEMAND,                 // get an element from the current position
    SA_RETURN,                 // jump one position back
    SA_REWIND,                 // rewind to the beginning of the queue
    SA_BURNITDOWN,             // destroy the storage, free all memory allocated
};

int lexAnalyzerStoreman(int32_t action) {

}

enum workerStates {
    WS_NONE,
    WS_STRING,
    WS_NUMBER,
    WS_WORD,
    WS_CONTROLSYMBOL,
    WS_COMMENTARY,
};


#define PUSHTOKEN_STR(_type, _ptr) { \
    token_t _localToken; \
    _localToken.type = _type; \
    _localToken.ptr = lolStrdup(_ptr); \
    stack_pushCopy(tokenStack, &_localToken, sizeof(_localToken)); \
}

#define PUSHTOKEN_INT(_type, _num) { \
    token_t _localToken; \
    _localToken.type = _type; \
    _localToken.num = _num; \
    stack_pushCopy(tokenStack, &_localToken, sizeof(_localToken)); \
}

#define PROCESS_STACK() {if (processStackContent(charStack, tokenStack, state)) lolErrorNo(LERR_LEXICAL_ERROR);}

int processStackContent(stack_t *charStack, stack_t *tokenStack,
                        enum workerStates oldState) {
    char *charStackStart = charStack->array.ptr;
    size_t charStackLength = charStack->tip;
    char *charStackEnd = charStack->array.ptr + charStack->tip - 1;

    if (!charStackLength)
        return 1;

    if (oldState == WS_NUMBER) {
        int64_t number = strtoll(charStackStart, &charStackEnd, 10);
        PUSHTOKEN_INT(T_NUMBER, number);
    }
    else if (oldState == WS_WORD) {
        for (size_t i = 0; i < charStackLength; i++) {
            enum keywordTypes match = K_NOTYPE; //maybe i should use notype
            for (enum keywordTypes keyword = 1; i < K_LAST_KEYWORD; i++) {
                if (strncmp(charStackStart, keywords[keyword], strlen(keywords[keyword])) == 0) {
                    match = keyword;
                }
            }
            if (match == K_NOTYPE) {
                char *tmpPtr = charStackStart + i;
                for (size_t i = 0; i < charStackLength; i++) {
                    if (charStackStart[i] == ' ') {
                        charStackStart[i] = 0;
                        i++;
                        break;
                    }
                }
                PUSHTOKEN_STR(T_WORD, tmpPtr);
            }
            else {
                PUSHTOKEN_INT(T_KEYWORD, match);
            }
        }
    } else {
        fprintf(stderr, "token of an unknown type was requested for processing\n");
    }
    stack_flush(charStack);
    return 0;
}

int lexAnalyzerWorker(FILE *ifile) {
    stack_t *tokenStack = stack_init(512);
    void *tokenStackPtr = tokenStack->array.ptr;
    stack_t *charStack = stack_init(512);
    void *charStackPtr = charStack->array.ptr;
    int c;
    enum workerStates state = WS_NONE;
    while ((c = fgetc(ifile)) != EOF) {
        if (state == WS_STRING) {
            if (c == '"') {
                stack_pushChar(charStack, 0);
                PUSHTOKEN_STR(T_STRING, charStackPtr);
                stack_flush(charStack);
                state = WS_NONE;
            }
            else if (c == '\n') { // don't allow multi-line strings
                lolErrorNo(LERR_LEXICAL_ERROR);
            }
            else {
                stack_pushChar(charStack, c);
            }
        }
        else if (state == WS_COMMENTARY) {
            if (c == '\n') {
                state = WS_NONE;
                PUSHTOKEN_INT(T_CONTROL, C_SEPARATOR);
            }
        }
        else {
            if (c == '\n') { // separator
                PROCESS_STACK();
                PUSHTOKEN_INT(T_CONTROL, C_SEPARATOR);
                state = WS_NONE;
            } else if (c == '!') {
                PROCESS_STACK();
                PUSHTOKEN_INT(T_CONTROL, C_EXCLAMATION);
                state = WS_NONE;
            } else if (c == '?') {
                PROCESS_STACK();
                PUSHTOKEN_INT(T_CONTROL, C_QUESTION);
                state = WS_NONE;
            } else if (isdigit(c)) { // a number
                if (state != WS_NUMBER) {
                    PROCESS_STACK();
                    state = WS_NUMBER;
                }
                stack_pushChar(charStack, c);
            } else if (isalpha(c)) { // word
                if (state != WS_WORD) {
                    PROCESS_STACK();
                    state = WS_WORD;
                }
                stack_pushChar(charStack, c);
            } else if (c == ' ' || c == '\t') { // care about spaces in words
                if (state == WS_WORD) {
                    stack_pushChar(charStack, c);
                }
                else if (state != WS_NONE) {
                    PROCESS_STACK();
                    state = WS_NONE;
                }
            } else if (c == '"') { // string start
                PROCESS_STACK();
                state = WS_STRING;
            } else { // undefined -> error

            }
        }
    }
    return !!ferror(ifile); // returns 1 on input stream error
}

int main(void) {
    lexAnalyzerWorker(stdin);
    return EXIT_SUCCESS;
}
